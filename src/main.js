import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import {
  database,
  firebaseUpload,
  FirebaseMessaging,
} from "./plugins/firebase";
import { sleep } from "@/plugins/helper";
import moment from "moment";
import firebase from "firebase";
import { getDeviceFcmToken } from "./plugins/firebase";
import { Capacitor } from "@capacitor/core";

import VueMask from "v-mask";
Vue.use(VueMask);

import { PushNotifications } from "@capacitor/push-notifications";

var databasePrefix = "myapp";

Vue.mixin({
  computed: {
    $db() {
      return database;
    },
    $dbRef() {
      return database.ref();
    },
    $fcm() {
      return FirebaseMessaging;
    },
    $uid() {
      const user = firebase.auth().currentUser;
      return user ? user.uid : null;
    },
  },
  methods: {
    sleep,
    back() {
      history.back();
    },
    formatDate(date) {
      if (!date) return "";
      return moment(date).format("DD/MM/YYYY HH:mm");
    },
    async $dbWatcher(ref, func) {
      database.ref(`${databasePrefix}/${ref}`).on("value", (snapshot) => {
        var val = snapshot.val();
        if (!func) return;
        func(val || null);
      });
    },
    async $dbSet(ref, data) {
      return new Promise((resolve) => {
        return database.ref(`${databasePrefix}/${ref}`).set(data, (err) => {
          if (err) {
            alert(err.message);
            return resolve(false);
          }
          return resolve(true);
        });
      });
    },
    async $dbRemove(ref) {
      return new Promise((resolve) => {
        database
          .ref(`${databasePrefix}/${ref}`)
          .remove()
          .then(() => {
            return resolve(true);
          });
      });
    },
    async $dbGet(ref) {
      return new Promise((resolve) => {
        database
          .ref()
          .child(databasePrefix)
          .child(ref)
          .get()
          .then((snapshot) => {
            if (snapshot.exists()) {
              return resolve(snapshot.val());
            } else {
              console.log("No data available");
              return resolve(false);
            }
          })
          .catch((error) => {
            console.error(error);
            return resolve(false);
          });
      });
    },
    async $upload(file, name) {
      return await firebaseUpload(file, name);
    },
    async registerNativeNotificationToken() {
      let permStatus = await PushNotifications.checkPermissions();
      if (permStatus.receive === "prompt") {
        permStatus = await PushNotifications.requestPermissions();
      }
      if (permStatus.receive !== "granted") {
        console.log("User denied permissions!");
        return;
      }
      await PushNotifications.register();
    },
    async handleDeviceToken(deviceToken) {
      const user = firebase.auth().currentUser;
      if (!this.$uid || !deviceToken) return;
      setLocalStorage("FcmDeviceToken", deviceToken);
      await this.$dbSet(
        `deviceToken/${user.uid}/${this.$uid}/${deviceToken}`,
        moment().format()
      );
    },
    async registerNotificationToken() {
      try {
        if (!this.$uid) return;
        if (Capacitor.isNativePlatform()) {
          await this.registerNativeNotificationToken();
        } else {
          await this.registerWebNotificationToken();
        }
      } catch (err) {
        console.log(">>> registerNotificationToken Error", err.message);
      }
    },
    async registerWebNotificationToken() {
      var deviceToken = await getDeviceFcmToken();
      console.log("FCM Device Token", deviceToken);
      await this.handleDeviceToken(deviceToken);
    },
    async handleNotification(payload = {}) {
      // console.log(payload);
      this.playNof();
      // for iOS we also receive noti on the forground
      // so,no need to show the toast nitification
      if (Capacitor.getPlatform().toLowerCase() == "ios") return;
      if (!payload) return;
      var { notification } = payload;
      if (!notification) return;
      var { title, body } = notification;
      if (!title || !body) return;
      var message = `${title}: ${body}`;
      if (this.isiOS) message = body;
      this.showInfo(message);
    },
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");

import { setLocalStorage } from "./plugins/helper";
