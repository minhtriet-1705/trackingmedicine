import {
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
} from "@/plugins/helper";

function getStoredUser() {
  var userStr = getLocalStorage("BLE_APP_USER_OBJECT");
  if (userStr) {
    return JSON.parse(userStr);
  }
  return {
    id: "",
    name: "",
    gender: "",
    date: "",
    profileID: "",
  };
}

const state = {
  user: getStoredUser(),
};

const getters = {
  getUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return (state.user.id && state.user.name) || false;
  },
};

const mutations = {
  SET_USER_PROFILE(state, payload) {
    state.user = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_ID(state, payload) {
    state.user.id = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_FULLNAME(state, payload) {
    state.user.name = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_GENDER(state, payload) {
    state.user.gender = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_BIRTHDATE(state, payload) {
    state.user.date = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_PROFILEID(state, payload) {
    state.user.profileID = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  SET_USER(state, payload) {
    state.user = payload;
    var strUser = JSON.stringify(state.user);
    setLocalStorage("BLE_APP_USER_OBJECT", strUser);
  },
  LOGOUT(state) {
    removeLocalStorage("BLE_APP_USER_OBJECT");
    state.user = getStoredUser();
  },
};

export default {
  namespaced: "Authen",
  state,
  mutations,
  getters,
};
