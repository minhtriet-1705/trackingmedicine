// store.js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo: {
      name: "",
      date: "",
      age: "",
      gender: "",
      profileID: "",
    },
  },
  mutations: {
    updateUserInfo(state, payload) {
      state.userInfo = { ...state.userInfo, ...payload };
    },
  },
  actions: {
    setUserInfo({ commit }, userInfo) {
      commit("updateUserInfo", userInfo);
    },
  },
  getters: {
    getUserInfo: (state) => state.userInfo,
  },
});
