import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyC7JWSlLWSkkdCOYtOOp1eu72dzxrmSsmE",
  authDomain: "imoney-45427.firebaseapp.com",
  databaseURL: "https://imoney-45427-default-rtdb.firebaseio.com/",
  projectId: "imoney-45427",
  storageBucket: "imoney-45427.appspot.com",
  messagingSenderId: "152859217389",
  appId: "1:152859217389:web:77492e5aea93ada89debe4",
  measurementId: "G-NVJFDB3NP9",
};

export function getGcpGsUrl() {
  return `gs://${firebaseConfig.storageBucket}/public`;
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const database = firebase.database();
// firebase storage
// Get a reference to the storage service, which is used to create references in your storage bucket
var storage = firebase.storage();

// Create a storage reference from our storage service
var storageRef = storage.ref();

export async function firebaseUploadBlob(blob, prefix, fileName, onProgressFn) {
  //A Blob() is almost a File() - it's just missing the two properties below which we will add
  blob.lastModifiedDate = new Date();
  blob.name = fileName;
  var name = `${prefix}/${fileName}`;
  return firebaseUpload(blob, name, onProgressFn);
}

export async function firebaseUpload(file, name, onProgressFn) {
  var path = `public/${name}`;
  var uploadTask = storageRef.child(path).put(file);
  return new Promise((resolve, reject) => {
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      // on uploadString progress,
      function (snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        if (onProgressFn) {
          onProgressFn(progress);
        }
      },
      function (error) {
        return reject(error);
      },
      function () {
        // Upload completed successfully, now we can get the download URL
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          return resolve(downloadURL);
        });
      }
    );
  });
}

export async function firebaseUploadBase64(base64Str, name, onProgressFn) {
  var path = `public/${name}`;
  var uploadTask = storageRef.child(path).putString(base64Str, "data_url");
  return new Promise((resolve, reject) => {
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      // on upload progress,
      function (snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        if (onProgressFn) {
          onProgressFn(progress);
        }
      },
      function (error) {
        return reject(error);
      },
      function () {
        // Upload completed successfully, now we can get the download URL
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          return resolve(downloadURL);
        });
      }
    );
  });
}

// firebase messaging
var fcmWebPushToken =
  "BKc6-XOmrt3WzCQuAFcURCLwzIrYzkaTT-2zk-OuLA4-ZJE4Qa3mjDXJSgsla6I44ImOgxf77DfLxiw922y_hHw";

function getFcmInstance() {
  if (firebase.messaging.isSupported()) {
    return firebase.messaging();
  } else {
    return {
      onMessage: async () => {
        return true;
      },
    };
  }
}

export const FirebaseMessaging = getFcmInstance();

export async function getDeviceFcmToken() {
  return new Promise((resolve, reject) => {
    // Check browser support first hand
    if (firebase.messaging.isSupported()) {
      console.log("This browser có hỗ trợ Notification. Hooray!");
      FirebaseMessaging.getToken({ vapidKey: fcmWebPushToken })
        .then((currentToken) => {
          if (currentToken) {
            return resolve(currentToken);
          } else {
            console.error(
              "No registration token available. Request permission to generate one."
            );
          }
        })
        .catch((err) => {
          console.error("An error occurred while retrieving token. ", err);
          return reject(err);
        });
    } else {
      return resolve(null);
    }
  });
}
