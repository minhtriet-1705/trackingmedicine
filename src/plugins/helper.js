import moment from "moment";

export const isAuthenticated = () => {
  return getLocalStorage(process.env.VUE_APP_TOKEN_NAME) || false;
};

export function uuidv4() {
  var code = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
    /[xy]/g,
    function (c) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    }
  );
  return `${moment().utc().format("YYYYMMDDHHmmss")}-${code}`;
}

export function setCookie(cname, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
  const name = cname;
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return "";
}

export const deleteCookie = (cname) => {
  const name = cname;
  document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

export function setLocalStorage(lname, lvalue) {
  localStorage.setItem(lname, lvalue);
  return true;
}

export function removeLocalStorage(lname) {
  localStorage.removeItem(lname);
  return true;
}

export function getLocalStorage(lname) {
  const stringData = localStorage.getItem(lname);
  if (!stringData) {
    return null;
  }
  return stringData;
}

export function parseLocalStorage(lname) {
  const stringData = getLocalStorage(lname);
  if (!stringData) {
    return null;
  }
  return JSON.parse(stringData);
}

export function capitalize(s) {
  if (!s) return null;
  return s[0].toUpperCase() + s.slice(1);
}

export const convertPriceString = (val, isVND = true, forceMinus = false) => {
  if (val == 0) return "0.000" + (isVND ? " VND" : "");
  if (!val || isNaN(val)) return "";

  // if val is actually < 0 => forceMinus = true
  if (val < 0) {
    return convertPriceString(Math.abs(val), isVND, true);
  }

  const valStr = typeof val === "string" ? val : val.toString();
  const valDec = valStr.split(".")[0];

  var result =
    valDec.replace(/./g, function (c, i, a) {
      return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
    }) + (isVND ? " VND" : "");

  return `${forceMinus ? "- " : ""}` + result;
};

export const deconvertPriceString = (str) => {
  if (!str) return 0;
  var isMinus = str.includes("-");
  var numb = str.match(/\d/g);
  numb = numb.join("");
  if (isMinus) {
    numb = "-" + numb;
  }
  return parseInt(numb);
};

export const getNumberFromString = (str) => {
  if (!str) return 0;
  var numb = str.match(/\d/g);
  numb = numb.join("");
  return parseInt(numb);
};

export const formatNumber = (val) => {
  if (!val || isNaN(val)) return "";

  const valStr = typeof val === "string" ? val : val.toString();
  const valDec = valStr.split(",")[0];
  // const valExponent = valStr.split('.')[1] ? ',' + valStr.split('.')[1] : ''

  return valDec.replace(/,/g, function (c, i, a) {
    return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "," + c : c;
  });
};

export const currencyFormat = (num, position, char) => {
  if (num == null) {
    return 0;
  }

  var number = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");

  switch (position) {
    case "head":
      number = char + number;
      break;
    case "end":
      number = number + char;
      break;

    default:
      break;
  }

  return number;
};

/**
 * Convert a vietnamese characters into alphabet characters to compare easily
 * @param {*} input
 * @return string alphabet
 */
export const transliterateChar = (input) => {
  var mappings = {
    ĂÂÀẰẦẢẲẨÃẴẪÁẮẤẠẶẬ: "A",
    ÊÈỀẺỂẼỄÉẾẸỆ: "E",
    ÌỈĨÍỊ: "I",
    ÔƠÒỒỜỎỔỞÕỖỠÓỐỚỌỘỢ: "O",
    ƯÙỪỦỬŨỮÚỨỤỰ: "U",
    ỲỶỸỴ: "Y",
    ăâàằầảẳẩãẵẫáắấạặậ: "a",
    êèềẻểẽễéếẹệ: "e",
    ìỉĩíị: "i",
    ôơòồờỏổởõỗỡóốớọộợ: "o",
    ưùừủửũữúứụự: "u",
    ỳỷỹỵ: "y",
  };
  for (const c of input.split(""))
    for (const mapping in mappings)
      if (mapping.includes(c)) input = input.replaceAll(c, mappings[mapping]);
  return input;
};

export const sleep = (milliseconds = 100) => {
  return new Promise((resolve) => {
    var timeout = setTimeout(() => {
      clearTimeout(timeout);
      resolve();
    }, milliseconds);
  });
};

// convert long string to part of string, between word is space
export const convertLongWordToEasyWord = (stringInput = "", result) => {
  let string = stringInput;
  let resultString = result;

  for (let i = 0; i < string.length; i++) {
    let char = string.charAt(i);
    if (char == " ") {
      i = i + 1;
      char = string.charAt(i).toUpperCase(); // get next char
    }
    if (char == char.toUpperCase()) {
      resultString =
        i == 0 && resultString == "" ? char : `${resultString} ${char}`;
    } else {
      if (i == 0 && resultString == "") {
        resultString = char.toUpperCase();
      } else {
        resultString += `${char}`;
      }
    }
    if (i + 1 == string.length) {
      return resultString;
    }
    let nextString = string.slice(i + 1, string.length);
    return convertLongWordToEasyWord(nextString, resultString);
  }
  return result;
};

export const calculateAverage = (items) => {
  var sum = 0;
  for (var i = 0; i < items.length; i++) {
    sum += parseInt(items[i], 10); //don't forget to add the base
  }
  return sum / items.length;
};

export const getUniqueValues = (items) => {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  return items.filter(onlyUnique);
};
export default {
  restructuredText: null,
};
