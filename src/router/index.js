import Vue from "vue";
import VueRouter from "vue-router";
import LogIn from "../components/LogIn.vue";
import SignUp from "../components/SignUp.vue";
import SettingPage from "../views/SettingPage.vue";
import MedicineInput from "../views/MedicineInput.vue";
import MedicineScan from "../views/MedicineScan.vue";
import HistoryPage from "../views/HistoryPage.vue";
import HomePage from "../views/HomePage.vue";
import MedicineScanTer from "../views/MedicineScanTer.vue";
import MedicineView from "../views/MedicineView";
import MedicineFillAfterScan from "../components/MedicineFillAfterScan.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "LogIn",
    component: LogIn,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/setting",
    name: "SettingPage",
    component: SettingPage,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/medicineinput",
    name: "MedicineInput",
    component: MedicineInput,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/medicinefill",
    name: "MedicineFill",
    component: MedicineFillAfterScan,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/medicinescan",
    name: "MedicineScan",
    component: MedicineScan,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/medicinescanter",
    name: "MedicineScanTer",
    component: MedicineScanTer,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/medicine/:id",
    name: "MedicineView",
    component: MedicineView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/history",
    name: "HistoryPage",
    component: HistoryPage,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/",
    name: "HomePage",
    component: HomePage,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
export default router;
